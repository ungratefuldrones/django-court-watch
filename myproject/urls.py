"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from rest_framework import routers
from core import views
from core.admin import admin_site
# import nexus

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'cases', views.CaseViewSet)


urlpatterns = [
    url(r'^case-explorer/', admin_site.urls),
    url(r'^admin/', admin.site.urls),
    # url(r'^nexus/', include(nexus.site.urls)),
]
if settings.DEBUG == True:
    urlpatterns += [
	    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
	        'document_root': settings.MEDIA_ROOT,
	    }),
	    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
	        'document_root': settings.STATIC_ROOT,
	    }),
        url(r'^', include(router.urls)),
	    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	    # url(r'^cases/$', views.case_list),
	    # url(r'^cases/(?P<pk>[0-9]+)$', views.case_detail),
		url(r'^cases/$', views.CaseView.as_view(), name='case-list')

	]