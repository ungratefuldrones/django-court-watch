from django.contrib import admin
from django.db.models import Q

class ChargeFiledListFilter(admin.SimpleListFilter):
    title = 'charge filed'
    parameter_name = 'charge'

    def lookups(self, request, model_admin):

        return (
            ('APO', 'Assault on a Police Officer'),
        )

    def queryset(self, request, queryset):

        if self.value() == 'APO':
            try:
                return queryset \
                    .filter(Q(docketevents__data__contains=[{"charge":"Assault on a Police Officer"}]) \
                        & Q(docketevents__description__contains="Charges Filed"))
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()

class OtherListFilter(admin.SimpleListFilter):
    title = 'issue'
    parameter_name = 'issue'

    def lookups(self, request, model_admin):
        return (
            ('Brady', 'Brady Issues'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'Brady':
            try:
                return queryset.filter(docketevents__h_data__message__contains="Brady").distinct()
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()

"""
By disposition

All
Closed-No Papered
Consolidated-Indicted/Consolidated W/Another Case
Dismissed
Dismissed-DWP
Dismissed-Grand Jury
Dismissed-Nolle-Diversion
Dismissed-Nolle-Prosequi
Dismissed-No Probable Cause
Dismissed-Plea Agreement
Dismissed-Prosecution Abated
Guilty-904 Guilty Plea
Guilty-Court Trial*
Guilty-Jury Trial
Guilty- Plea Judgment Guilty
Not Guilty-Acquittal
Not Guilty-Court Trial*
Not Guilty-Jury Trial
Undisposed
"""

"""
CLEAN THIS UP 
"""

class DispositionListFilter(admin.SimpleListFilter):
    title = 'disposition'
    parameter_name = 'disposition'

    def lookups(self, request, model_admin):
        return (
            ('Closed', 'Closed'),
            ('Consolidated', 'Consolidated'), 
            ('Dismissed', 'Dismissed'),
            ('Guilty', 'Guilty'),
            ('Not Guilty', 'Not Guilty'),
            ('Undisposed', 'Undisposed'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'Undisposed':
            try:
                return queryset.filter(disposition__in=["Undisposed"])
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()
        if self.value() == 'Closed':
            try:
                return queryset.filter(disposition__in=["Closed-No Papered"])
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()
        if self.value() == 'Consolidated':
            try:
                return queryset.filter(disposition__in=["Consolidated-Indicted/Consolidated W/Another Case"])
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()
        if self.value() == 'Dismissed':
            try:
                return queryset.filter(disposition__in=["Dismissed",
                    "Dismissed-DWP",
                    "Dismissed-Grand Jury",
                    "Dismissed-Nolle-Diversion",
                    "Dismissed-Nolle-Prosequi",
                    "Dismissed-No Probable Cause",
                    "Dismissed-Plea Agreement",
                    "Dismissed-Prosecution Abated"])
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()
        if self.value() == 'Guilty':
            try:
                return queryset.filter(disposition__in=["Guilty-904 Guilty Plea",
                    "Guilty-Court Trial*",
                    "Guilty-Jury Trial",
                    "Guilty- Plea Judgment Guilty"])
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()
        if self.value() == 'Closed':
            try:
                return queryset.filter(disposition__in=["Not Guilty-Acquittal", "Not Guilty-Court Trial*", "Not Guilty-Jury Trial"])
            except Exception as e:
                print(e)
                import pdb; pdb.set_trace()