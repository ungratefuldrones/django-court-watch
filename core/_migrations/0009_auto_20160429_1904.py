# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-29 18:04
from __future__ import unicode_literals
from django.contrib.postgres.operations import HStoreExtension


import django.contrib.postgres.fields.hstore
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20160429_1722'),
    ]

    operations = [
        HStoreExtension(),
        migrations.AlterField(
            model_name='docketevent',
            name='data',
            field=django.contrib.postgres.fields.hstore.HStoreField(),
        ),
        
    ]
