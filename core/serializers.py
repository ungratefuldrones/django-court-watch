from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Case, DocketEvent, Party, ScheduleEvent, Charge


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class DocketEventSerializer(serializers.ModelSerializer):
    # date = serializers.DateTimeField(format="iso-8601"
    class Meta:
        model = DocketEvent
        fields = ('date', 'description', 'h_data')

class PartySerializer(serializers.ModelSerializer):
    class Meta:
        model = Party
        fields = ('name', 'party_type', 'attorney')


class ScheduleEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleEvent
        fields = ('date', 'description', )

class ChargeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Charge
        fields = ('position', 'charge', )

class CaseSerializer(serializers.HyperlinkedModelSerializer):
    docketevents = DocketEventSerializer(many=True)
    parties = PartySerializer(many=True)
    scheduleevents = ScheduleEventSerializer(many=True)
    charges = ChargeSerializer(many=True)
    
    class Meta:
        model = Case
        fields = ('case_type', 'case_year', 'case_id', 
            'file_date', 'status', 'status_date', 'disposition', 'disposition_date',
            'docketevents', 'parties', 'scheduleevents', 'charges', 'raw_html')


    def create(self, validated_data):
        charges_data = validated_data.pop('charges')
        scheduleevents_data = validated_data.pop('scheduleevents')
        docketevents_data = validated_data.pop('docketevents')
        parties_data = validated_data.pop('parties')
        case = Case.objects.create(**validated_data)
        for charge_data in charges_data:
            Charge.objects.create(case=case, **charge_data)
        for docketevent_data in docketevents_data:
            DocketEvent.objects.create(case=case, **docketevent_data)
        for party_data in parties_data:
            Party.objects.create(case=case, **party_data)
        for scheduleevent_data in scheduleevents_data:
            ScheduleEvent.objects.create(case=case, **scheduleevent_data)
        return case

    def update(self, instance, validated_data):
        instance.delete()
        charges_data = validated_data.pop('charges')
        scheduleevents_data = validated_data.pop('scheduleevents')
        docketevents_data = validated_data.pop('docketevents')
        parties_data = validated_data.pop('parties')
        case = Case.objects.create(**validated_data)
        for charge_data in charges_data:
            Charge.objects.create(case=case, **charge_data)
        for docketevent_data in docketevents_data:
            DocketEvent.objects.create(case=case, **docketevent_data)
        for party_data in parties_data:
            Party.objects.create(case=case, **party_data)
        for scheduleevent_data in scheduleevents_data:
            ScheduleEvent.objects.create(case=case, **scheduleevent_data)
        return case