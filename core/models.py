from django.db import models
from django.contrib.postgres.fields import JSONField, HStoreField, ArrayField
from django.utils.safestring import mark_safe
import re

# Create your models here.
class Case(models.Model):
	DOMESTIC_VIOLENCE = 'DVM'
	MISDEMEANOR = 'CMD'
	FELONY_1 = 'CF1'
	FELONY_2 = 'CF2'
	FELONY_3 = 'CF3'

	
	CASE_TYPE = (
		(DOMESTIC_VIOLENCE, 'Domestic Violence'),
		(FELONY_1, 'Felony I'),
		(FELONY_2, 'Felony II'),
		(FELONY_3, 'Felony III'),
		(MISDEMEANOR, 'Misdemeanor'),
	)
	
	case_year = models.IntegerField()
	case_id = models.CharField(max_length=20, primary_key=True, db_index=True)
	case_type = models.CharField(max_length=3, choices=CASE_TYPE)
	status = models.CharField(max_length=255, blank=True, null=True)
	disposition = models.CharField(max_length=255, blank=True, null=True)
	file_date = models.DateField(null=True, blank=True)
	status_date = models.DateField(null=True, blank=True)
	disposition_date = models.DateField(null=True, blank=True)
	scrape_date = models.DateTimeField(auto_now_add=True)
	report = models.FileField(null=True, blank=True)
	raw_html = models.TextField(null=True, blank=True)

	@property
	def all_charges(self):
		try:
		    return mark_safe(', '.join([i.charge for i in self.charges.get_queryset()]))
		except:
			return 'No charges listed'


	def __str__(self): # __str__ for Python 3, __unicode__ for Python 2
		return "%s" % (self.case_id) # str(self.case_id).zfill(7)

	class Meta:
		unique_together = (("case_year", "case_type", "case_id"),)


class ScheduleEvent(models.Model):
	case = models.ForeignKey(Case, on_delete=models.CASCADE, related_name='scheduleevents')
	date = models.DateTimeField(null=True, blank=True)
	description = models.CharField(max_length=255, blank=True, null=True)

class Charge(models.Model):
	case = models.ForeignKey(Case, on_delete=models.CASCADE, related_name='charges')
	position = models.IntegerField(null=True, blank=True)
	charge = models.CharField(max_length=255, blank=True, null=True)


class Party(models.Model):
	case = models.ForeignKey(Case, on_delete=models.CASCADE, related_name='parties')
	name = models.CharField(max_length=255, null=True, blank=True)
	# alias = None # array?
	party_type = models.CharField(max_length=255, null=True, blank=True)
	attorney = models.CharField(max_length=255, null=True, blank=True)

	def __str__(self):
		return self.party_type

	class Meta:
		verbose_name_plural = "parties"


class Document(models.Model):
	pass

class Annotation(models.Model):
	pass



class DocketEvent(models.Model):
	case = models.ForeignKey(Case, on_delete=models.CASCADE, related_name='docketevents')
	date = models.DateField(null=True, blank=True)
	description = models.CharField(max_length=255)
	data = JSONField(db_index=True, null=True, blank=True)
	h_data = HStoreField(null=True, blank=True) 

	def info(self):
	    try:

	    	# return dict(self.h_data)['message'].replace(self.description, '', 1)
	    	return mark_safe("""<div style="display:flex">
	    		<div style="display:table;margin-right:1.2em;">{0}</div>
	    			<div style="width:100%;padding-left:1.2em;border-left:1px solid gainsboro">{1}</div>
	    		</div>""".format(self.date, re.sub(r'^(<br>)+', '', dict(self.h_data)['message'].replace(self.description, '', 1).strip())))
	    except:
	    	return ''

	def __str__(self):
		return "{1}".format(self.date, self.description)
	