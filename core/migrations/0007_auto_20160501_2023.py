# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-01 19:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20160501_2019'),
    ]

    operations = [
        migrations.AddField(
            model_name='case',
            name='disposition',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='case',
            name='disposition_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='case',
            name='file_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='case',
            name='status',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='case',
            name='status_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
