from django import forms

class ReadOnlyDocketEventInfoWidget(forms.TextInput):
    def render(self, name, value, attrs=None):
         if value is None:
             value = ''
         else:
            value = json.loads(value)
         final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
         if value != '':
             # Only add the 'value' attribute if a value is non-empty.
             final_attrs['value'] = self._format_value(value)
         try:
             return mark_safe(u'<div>%s</div>' % value['message'])
         except:
             return ''