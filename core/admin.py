from django.contrib import admin
from django.db import models
from django.contrib.auth.forms import AuthenticationForm
from reversion.admin import VersionAdmin
from import_export.admin import ImportExportActionModelAdmin
from import_export import resources
from django.http import HttpResponse

from .models import Case, Party, DocketEvent, ScheduleEvent, Charge
from .filters import ChargeFiledListFilter, OtherListFilter, DispositionListFilter


# # import-export resource
# class CaseActionModelAdmin(ImportExportActionModelAdmin):
#     model = Case
#     fields = ('raw_html',)    

def export_as_html_action(description="Export selected objects as HTML file",
                         fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_html(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset
        
        response = HttpResponse()
        response['Content-Disposition'] = 'attachment; filename=%s.html' % str(opts).replace('.', '_')
        
        for obj in queryset:
            response.content += getattr(obj, 'raw_html').encode('utf-8')
        return response
    export_as_html.short_description = description
    return export_as_html


class PartyInline(admin.TabularInline):
    model = Party
    extra = 0
    readonly_fields = ('name', 'party_type', 'attorney')

    fieldsets = [
        (None, {'fields': ['name', 'attorney'],}),
    ]

    def get_queryset(self, request):
        qs = super(PartyInline, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(party_type__icontains="defendant")


class ChargeInline(admin.TabularInline):
    model = Charge
    extra = 0
    fieldsets = [
        (None, {'fields': ['charge'],}),
    ]

class ScheduleInline(admin.TabularInline):
    model = ScheduleEvent
    extra = 0
    
    fieldsets = [
        (None, {'fields': ['date', 'description'],}),
    ]
        
class DocketInline(admin.TabularInline):
    model = DocketEvent
    extra = 0
    readonly_fields = ('info',)
    classes = ('extrapretty',)

    fieldsets = [
        # (None, {'fields': ['date','description', 'h_data'],}),
        (None, {'fields': ['info'],}),
    ]

    # def formfield_for_dbfield(self, db_field, **kwargs):
    #     if db_field.name == 'h_data':
    #         kwargs['widget'] = ReadOnlyDocketEventInfoWidget()
    #     return super(DocketInline,self).formfield_for_dbfield(db_field,**kwargs)


# class ScheduleAdmin(admin.ModelAdmin):
#     list_display = ('date', 'description')
#     # readonly_fields = ('case',)

class DocketEventAdmin(admin.ModelAdmin):
    list_display = ('case', 'date', 'description', 'h_data')
    list_filter = ('date', 'description',)
    search_fields = ['h_data']


class CaseAdmin(VersionAdmin):
    actions_on_top = True
    list_display = ('__str__', 'disposition', 'file_date','case_type', 'scrape_date')
    inlines = [PartyInline, DocketInline]
    readonly_fields = ("all_charges", 'status', 'disposition_date', 'file_date', 'disposition',)
    
    fieldsets = [

        ('Case Information', {'fields': 
            ['status', 'file_date', 'disposition_date', 'disposition', 'all_charges' ], 
            'classes': ['extrapretty']}),
        ('Investigation', {'fields': ['report'], 'classes': ['collapse']}),
        
    ]
    list_filter = [OtherListFilter, 'case_year', DispositionListFilter, 'case_type', ChargeFiledListFilter] # 'charges__charge'
    # filter_horizontal = ('case_type',)
    search_fields = ['docketevents__h_data', 'parties__name', "case_id"]
    actions = [export_as_html_action("Export selected emails as HTML file", fields=['raw_html'], header=False),]

    save_on_top = True


class CaseAdminProtected(CaseAdmin):
    # def has_module_permission(self, request):
    #     return False

    def get_actions(self, request):
        #Disable delete
        actions = super(CaseAdminProtected, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False


class CaseManagerSite(admin.AdminSite):
    site_header = 'Court Watch'
    site_title = 'Court Watch'
    site_url = None
    index_title = None

    login_form = AuthenticationForm

    def has_permission(self, request):
        """
        Removed check for is_staff.
        """
        return request.user.is_active

    class Media:
        css = {
            "all": ("case_admin.css",)
        }

admin_site = CaseManagerSite(name='myadmin')




admin.site.register(Case, CaseAdmin)
admin_site.register(Case, CaseAdminProtected)
admin.site.register(DocketEvent, DocketEventAdmin)

# admin_site.register(ScheduleEvent, ScheduleAdmin)
# admin_site.register(DocketEvent, DocketEventAdmin)